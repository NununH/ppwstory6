from django.shortcuts import redirect, render

from .forms import answers_Form
from .models import Answers

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = answers_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('greetings:index')
    else:
        form = answers_Form()

    context = {
        'form': form,
        'all_answers': Answers.objects.all().order_by('-id'),
    }

    return render(request, 'greetings/index.html', context)
