from django import forms

from .models import Answers

class answers_Form(forms.ModelForm):
    class Meta:
        model = Answers
        fields = ['answers',]
