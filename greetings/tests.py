from selenium import webdriver
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import answers
from .forms import answers_Form

# Create your tests here.
class GreetingsUnitTest(TestCase):
    
    def test_greetings_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_greetings_url_isnt_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 404)
    
    def test_greetings_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = answers_Form()
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())
    
    def test_form_validation_for_blank_items(self):
        form = answers_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_greetings_success_and_render_the_result(self):
    	test = 'Anonymous'
        response_post = Client().post('/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_greetings_error_and_render_the_result(self):
    	test = 'Anonymous'
        response_post = Client().post('/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


class GreetingsFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_index(self):
        self.selenium.get(self.live_server_url + '/')

        self.assertInHTML('Halo, apa kabar?', self.selenium.page_source)

        status = self.selenium.find_element_by_id('id_status')
        button = self.selenium.find_element_by_tag_name('button')

        status.send_keys('Coba Coba')
        button.click()

        self.assertInHTML('Coba Coba', self.selenium.page_source)
